select @@ServerName AS Server ,
DB_NAME() AS DBName, OBJECT_SCHEMA_NAME(p.object_id) AS SchemaName ,
OBJECT_NAME(p.object_id) AS TableName, i.Type_Desc,
i.Name AS IndexUsedForCounts, SUM(p.Rows) AS Rows
from sys.partitions p
JOIN sys.indexes i ON i.object_id = p.object_id AND i.index_id = p.index_id
where i.type_desc IN ( 'CLUSTERED') AND OBJECT_SCHEMA_NAME(p.object_id) <> 'sys'
group by p.object_id,
i.type_desc,
i.Name
ORDER BY SchemaName,
TableName;


select @@ServerName AS Server ,
DB_NAME() AS DBName, OBJECT_SCHEMA_NAME(p.object_id) AS SchemaName ,
OBJECT_NAME(p.object_id) AS TableName, i.Type_Desc,
i.Name AS IndexUsedForCounts, SUM(p.Rows) AS Rows
from sys.partitions p
JOIN sys.indexes i ON i.object_id = p.object_id AND i.index_id = p.index_id
where i.type_desc IN ( 'NONCLUSTERED') AND OBJECT_SCHEMA_NAME(p.object_id) <> 'sys'
group by p.object_id,
i.type_desc, i.Name
ORDER BY SchemaName,
TableName;