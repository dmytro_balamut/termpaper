declare @path_restore NVARCHAR(MAX)
set @path_restore = 'D:\Program Files\Microsoft SQL Server\MSSQL15.PORTAL_SERVER\MSSQL\Backup\PortalDB.bak'
restore database PortalDB
from disk = @path_restore
with recovery, replace;