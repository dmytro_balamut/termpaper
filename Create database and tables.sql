---------------��������� ���� �����----------------
CREATE DATABASE PortalDB
USE PortalDB
---------------������� �����----------------
CREATE TABLE Roles
(Id int not null primary key,
		[Name] nvarchar(MAX) not null)

---------------������� ������������----------------
CREATE TABLE Users
(Id int not null primary key,
		Surname nvarchar(MAX) not null,
		Firstname nvarchar(MAX) not null,
		Lastname nvarchar(MAX) not null,
		Email nvarchar(MAX) not null,
		[Date] datetime not null,
		[Login] nvarchar(MAX) not null,
		[Password] nvarchar(MAX) not null,
		RoleID int foreign key references Roles(Id))

---------------������� ���������� ����----------------
CREATE TABLE Formofstudy
(Id int not null primary key,
		[Name] nvarchar(MAX) not null)

---------------������� �����----------------
CREATE TABLE Course
(Id int not null primary key,
		[Name] nvarchar(MAX) not null)

---------------������� ���� ����----------------
CREATE TABLE NameGroup
(Id int not null primary key,
		[Name] nvarchar(MAX) not null,
		[Date] datetime not null,
		UserID int foreign key references Users(Id),
		CourseID int foreign key references Course(Id),
		FormofstudyID int foreign key references Formofstudy(Id))

---------------������� ��'������ ���� �� ��������----------------
CREATE TABLE UserGroup
(Id int not null primary key,
		NameGroupID int not null,
		UserID int foreign key references Users(Id))

---------------������� �������� �����----------------
CREATE TABLE Commission
(Id int not null primary key,
		[Name] nvarchar(MAX) not null)

---------------������� ��'������ �������� ����� �� ����������----------------
CREATE TABLE CommissionUser
(Id int not null primary key,
		UserID int not null,
		CommissionID int foreign key references Commission(Id))

---------------������� ��������----------------
CREATE TABLE Discipline
(Id int not null primary key,
		[Name] nvarchar(MAX) not null,
		CommissionID int foreign key references Commission(Id))

---------------������� ��'������ ���������� �� ��������----------------
CREATE TABLE UserDiscipline
(Id int not null primary key,
		DisciplineID int foreign key references Discipline(Id),
		UserID int foreign key references Users(Id))

---------------������� ���������� ���� ----------------

CREATE TABLE PracticalWork
(Id int not null primary key,
		[Name] nvarchar(MAX) not null,
		[DateFirst] datetime not null,
		[DateLast] datetime not null,
		DisciplineID int foreign key references Discipline(Id))

---------------������� ���� ----------------
CREATE TABLE Rating
(Id int not null primary key,
		Mark float not null,
		UserID int not null,
		PracticalWorkID int foreign key references PracticalWork(Id))

---------------������� �������� ----------------
CREATE TABLE [File]
(Id int not null primary key,
		Way nvarchar(MAX) not null,
		PracticalWorkID int foreign key references PracticalWork(Id))
