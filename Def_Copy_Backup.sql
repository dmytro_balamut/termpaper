declare @path NVARCHAR(MAX)
set @path = 'D:\Program Files\Microsoft SQL Server\MSSQL15.PORTAL_SERVER\MSSQL\Backup\PortalDB.bak'
backup database PortalDB
to disk = @path with differential,
NOFORMAT, NOINIT, NAME = 'PortalDB Backup', SKIP, NOREWIND, NOUNLOAD, STATS
= 10;