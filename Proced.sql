CREATE PROCEDURE DropStudent
    @Id Int
AS
DELETE Users WHERE Id = @Id
DELETE UserGroup WHERE UserID = @Id

CREATE PROCEDURE DropVicladach
    @Id Int
AS
DELETE Users WHERE Id = @Id
DELETE UserDiscipline WHERE UserID = @Id

