select Commission.Name, Users.Surname, Users.Firstname, Users.Lastname FROM Commission 
inner join CommissionUser ON Commission.Id = CommissionUser.CommissionID
inner join Users ON CommissionUser.UserID = Users.Id

SELECT NameGroup.Name, Users.Surname, Course.Name, Formofstudy.Name FROM Course inner join NameGroup ON Course.Id = NameGroup.CourseID
inner join Users ON NameGroup.UserID = Users.Id inner join Formofstudy ON NameGroup.FormofstudyID = Formofstudy.Id

SELECT NameGroup.Name, Users.Id, Users.Surname, Users.Firstname, Users.Lastname
FROM NameGroup 
inner join UserGroup ON NameGroup.Id = UserGroup.NameGroupID
inner join Users on UserGroup.UserID = Users.Id
WHERE NameGroup.Name LIKE '%П%'

select Discipline.Name, Users.Surname, Users.Firstname, Users.Lastname from Discipline inner join UserDiscipline
on Discipline.Id = UserDiscipline.DisciplineID
inner join Users on UserDiscipline.UserID = Users.Id
WHERE Discipline.Name LIKE '%Веб%'